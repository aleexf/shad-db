#pragma once

// Client executes a number of client transactions, according to
// the given list of ClientTransactionSpec.

#include <cstdint>
#include <memory>
#include <vector>

#include "client_transaction.h"
#include "discovery.h"
#include "env.h"
#include "retrier.h"

class Client : public IActor {
public:
  Client(ActorId id, std::vector<ClientTransactionSpec> tx_specs,
         const Discovery *discovery, EnvProxy env);

  const ClientTransaction &get_tx(int index) const;

  // Actor interface implementation.
  virtual ActorId get_id() const override;
  virtual void on_tick(Clock &clock, std::vector<Message> messages) override;

private:
  const ActorId id_;
  EnvProxy env_;

  RetrierExpBackoff retrier_;
  std::vector<std::unique_ptr<ClientTransaction>> transactions_;

  TransactionId get_message_txid(const Message &msg) const;
  void report_unexpected_message(const Message &msg) const;
};
