#include <cassert>

#include "storage.h"

void TestStorageSimple() {
  Storage storage;

  assert(storage.get_at(1, 1) == DEFAULT_VALUE);
  assert(storage.get_last(1) == nullptr);

  storage.apply(1 /* timestamp */, 1 /* txid */, 1 /* key */, 2 /* value */);
  storage.apply(2 /* timestamp */, 2 /* txid */, 1 /* key */, 3 /* value */);

  assert(storage.get_at(0 /* timestamp */, 1 /* key */) == DEFAULT_VALUE);
  assert(storage.get_at(1 /* timestamp */, 1 /* key */) == 2);
  assert(storage.get_at(2 /* timestamp */, 1 /* key */) == 3);
  assert(storage.get_at(5 /* timestamp */, 1 /* key */) == 3);
}
