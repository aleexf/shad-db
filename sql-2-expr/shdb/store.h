#pragma once

#include "bufferpool.h"
#include "schema.h"
#include "statistics.h"
#include "table.h"

#include <filesystem>

namespace shdb {

class Store
{
    std::shared_ptr<BufferPool> buffer_pool;
    std::filesystem::path path;

public:
    Store(const std::filesystem::path &path, FrameIndex frame_count, std::shared_ptr<Statistics> statistics);

    void create_table(const std::filesystem::path &name);
    std::shared_ptr<Table> open_table(const std::filesystem::path &name, PageProvider provider);
    void remove_table(const std::filesystem::path &name);
    bool check_table_exists(const std::filesystem::path &name);
};

}    // namespace shdb
