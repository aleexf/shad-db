#pragma once

#include "jit.h"

namespace shdb {

class For
{
    Jit &jit;

    llvm::Value *increment;
    llvm::PHINode *iterator;
    std::vector<llvm::Value **> variables;
    std::vector<llvm::PHINode *> phis;
    llvm::BasicBlock *loopbb;
    llvm::BasicBlock *outbb;

public:
    For(Jit &jit,
        llvm::Value *&index,
        llvm::Value *start,
        llvm::Value *end,
        std::vector<llvm::Value **> variables = {});
    ~For();
};

class If
{
    Jit &jit;

    std::vector<llvm::Value **> variables;
    std::vector<llvm::Value *> previous;
    llvm::BasicBlock *previousbb;
    llvm::BasicBlock *nextbb;

    friend class Else;

public:
    If(Jit &jit, llvm::Value *cond, std::vector<llvm::Value **> variables = {});
    ~If();
};

class Else
{
public:
    explicit Else(If &head);
};

}    // namespace shdb
